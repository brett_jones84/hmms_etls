USE [$(MAIN_DB)]
GO

/****** Object:  StoredProcedure [dbo].[IMSDataLoad]    Script Date: 9/3/2017 11:21:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('IMSDataLoad') IS NOT NULL
	DROP PROCEDURE  [dbo].[IMSDataLoad]
GO

CREATE procedure [dbo].[IMSDataLoad] as
begin

BEGIN TRANSACTION [Tran1]

BEGIN TRY

	-----------------------------------------------------------------
	-----------------tbl_ResMgr_Warehouse----------------------------
	-----------------------------------------------------------------

	delete dbo.tbl_ResMgr_Warehouse where name not like '%HMMS%';

	set identity_insert dbo.tbl_ResMgr_Warehouse ON;

	MERGE dbo.tbl_resmgr_warehouse AS TARGET 
	using [$(STAGING_DB)].dbo.deb_webims_today_stocklocations AS Source 
	ON ( target.warehouse_id = source.stocklocationcode ) 
	WHEN matched THEN 
	  UPDATE SET Target.NAME = source.stocklocationname + ', ' + source.stocklocationcode
	WHEN NOT matched BY target THEN 
	  INSERT (warehouse_id, 
			  NAME) 
	  VALUES (Source.stocklocationcode, 
			  source.stocklocationname + ', ' + source.stocklocationcode); 

	set identity_insert dbo.tbl_ResMgr_Warehouse OFF;


	--populate region information
	update w
	set region = v.residencyName
	from dbo.tbl_ResMgr_Warehouse w
	join dbo.vdot_vw_stock_locations v on v.stocklocationcode = w.Warehouse_ID;

	--populate any missing values so at least these are accessible with the picklist
	update dbo.tbl_ResMgr_Warehouse
	set region = 'Other'
	where region is null;

	--select * from tbl_ResMgr_Warehouse;
	-----------------------------------------------------------------




	-----------------------------------------------------------------
	-----------------tbl_ResMgr_Inventory----------------------------
	-----------------------------------------------------------------
	truncate table tbl_ResMgr_Inventory;

	set identity_Insert tbl_ResMgr_Inventory ON;

	 MERGE dbo.tbl_resmgr_inventory AS Target
	using (SELECT * FROM [dbo].[vdot_vw_webims_staged_inventory]) AS Source
	ON ( target.inventory_id = source.uniqueInventoryWithConditionId )
	WHEN matched AND (target.NAME <> source.description OR target.NAME <>
	source.NAME OR target.number <> source.stockno OR target.units <>
	source.unitsdesc OR
	target.quantity <> source.stockqty OR target.unitcharge <> source.unitcost OR
	target.perm_id <> source.permid) THEN
	  UPDATE SET target.NAME = source.NAME,
				 target.description = source.description,
				 target.number = source.stockno,
				 target.units = source.unitsdesc,
				 target.quantity = source.stockqty,
				 target.updatedby_id = -1,
				 target.updateddate = Getdate(),
				 target.unitcharge = source.unitcost,
				 target.perm_id = source.permid,
				 target.purchaseCost = source.unitcost
	WHEN NOT matched BY target THEN
	  INSERT (inventory_id, --1
			  NAME, --2
			  description, --3
			  number, --4
			  units, --5
			  quantity, --6
			  loggedby_id, --7
			  loggeddate, --8
			  updatedby_id, --9
			  updateddate, --10
			  mfr_id, --11
			  mfrNumber, --12 
			  splr_id, --13
			  type_id, --14
			  department_id, --15
			  program_id, --16
			  unitcharge, --17
			  perm_id, --18
			  isDeleted, --19
			  newRecord, --20 
			  splrNumber, --21
			  purchaseCost --22
	  )
	  VALUES(source.uniqueInventoryWithConditionId, --1
			 SUBSTRING(SOURCE.NAME,1,100), --2
			 source.description, --3
			 source.stockno, --4
			 source.unitsdesc, --5
			 source.stockqty, --6
			 -1, --7
			 Getdate(), --8
			 -1, --9
			 Getdate(), --10
			 -1, --11
			 source.nigp_code, --12 mfrNumber
			 -1, --13
			 -1, --14
			 -1, --15
			 -1, --16
			 source.unitcost, --17
			 source.permid, --18
			 0, --19
			 0, --20
			 SUBSTRING(SOURCE.condition,1,50), --21
			 source.unitcost --22
		);  

	set identity_Insert tbl_ResMgr_Inventory OFF;

	--select * from tbl_ResMgr_Inventory
	-----------------------------------------------------------------



	-----------------------------------------------------------------
	-----------------tbl_ResMgr_Inventory_Location-------------------
	-----------------------------------------------------------------
	truncate table dbo.tbl_ResMgr_Inventory_Location;

	MERGE dbo.tbl_resmgr_inventory_location AS Target
	using (SELECT stocklocationcode,
				  Sum(stockqty) AS STOCKQTY,
				  uniqueInventoryWithConditionId
		   FROM   dbo.vdot_vw_webims_staged_inv_at_location
		   GROUP  BY stocklocationcode,
					 uniqueInventoryWithConditionId) AS Source
	ON ( target.warehouse_id = source.stocklocationcode
		 AND target.inventory_id = source.uniqueInventoryWithConditionId )
	WHEN matched AND (target.onhand <> source.stockqty) THEN
	  UPDATE SET target.onhand = source.stockqty
	WHEN NOT matched BY target THEN
	  INSERT (warehouse_id,
			  onhand,
			  inventory_id)
	  VALUES(source.stocklocationcode,
			 source.stockqty,
			 source.uniqueInventoryWithConditionId);  

	--select * from dbo.tbl_ResMgr_Inventory_Location
	-----------------------------------------------------------------




	-----------------------------------------------------------------
	-----------------tbl_ResMgr_Inventory_Categories-----------------
	-----------------------------------------------------------------
	truncate table dbo.tbl_ResMgr_Inventory_Categories;

	set identity_Insert tbl_ResMgr_Inventory_Categories ON;

	merge tbl_ResMgr_Inventory_Categories as Target
	using (
			select	ROW_NUMBER() OVER(ORDER BY category ASC) AS category_id,
					category name, 
					count(*) c
			from [$(STAGING_DB)].DBO.WVS_WEBIMS_STOCKGROUPS
			where category is not null
			group by category
		) as Source
	on (target.category_id = source.category_id)
	when matched and (
		target.category_id <> source.category_id
		or target.name <> source.name
		or target.description <> source.name) 
	then
	update	set target.name = source.name,
			target.description = source.name
	when not matched by target then
	insert (category_id, name, description)
	values(source.category_id, source.name, source.name)
	;

	set identity_Insert tbl_ResMgr_Inventory_Categories OFF;

	--select * from tbl_ResMgr_Inventory_Categories;
	-----------------------------------------------------------------



	-----------------------------------------------------------------
	-----------------tbl_ResMgr_Inventory_subCategories--------------
	-----------------------------------------------------------------
	set identity_Insert tbl_ResMgr_Inventory_SubCategories ON;

	truncate table tbl_ResMgr_Inventory_SubCategories;

	merge tbl_ResMgr_Inventory_SubCategories as Target
	using (
			select distinct Cast(STOCKGROUP as int) id, category_id, GROUPDESCRIPTION name, GROUPDESCRIPTION description
			 from [$(STAGING_DB)].DBO.WVS_WEBIMS_STOCKGROUPS sg
			join tbl_ResMgr_Inventory_Categories c
				on c.name = sg.category
		) as Source
	on (target.id = source.id)
	when matched and (
		target.category_id <> source.category_id
		or target.name <> source.name
		or target.description <> source.description) 
	then
	update	set target.category_id = source.category_id,
			target.name = source.name,
			target.description = source.name
	when not matched by target then
	insert (id, category_id, name, description)
	values(source.id, source.category_id, source.name, source.description)
	;

	set identity_Insert tbl_ResMgr_Inventory_SubCategories OFF;

	select * from tbl_ResMgr_Inventory_SubCategories;
	-----------------------------------------------------------------



	-----------------------------------------------------------------
	--update actual category/subcategory values in tbl_ResMgr_Inventory
	-----------------------------------------------------------------
	with source as (
	select Cast(STOCKGROUP as int) stockgroup_int, * --STOCKGROUP, GROUPDESCRIPTION
	 from [$(STAGING_DB)].DBO.WVS_WEBIMS_STOCKGROUPS sg
	join tbl_ResMgr_Inventory_Categories c
		on c.name = sg.category
	)

	update I
	set Category_ID = c.Category_ID, I.SubCategory_ID = c.stockgroup_int
	from tbl_ResMgr_Inventory I
	join source c on c.stockgroup = SUBSTRING(i.Number, 1, 3);




	-----------------------------------------------------------------
	--------------dbo.tbl_ResMgr_Inventory_Transactions--------------
	-----------------------------------------------------------------
	with seed as (
		SELECT MAX(transaction_id) as highest_value
		from dbo.tbl_ResMgr_Inventory_Transactions
	)
	merge dbo.tbl_ResMgr_Inventory_Transactions as Target
	using (
		select	seed.highest_value + row_number() over (order by ril.inventory_id) as new_unique_id
				, ri.*
				, ril.Location_ID
				, ril.Warehouse_ID
				, ril.Aisle
				, ril.shelf
				, ril.bin
				, ril.OnHand
				, ril.Reserved
				, ril.Backordered
				, ril.Counted
		from dbo.tbl_ResMgr_Inventory_Location ril
		join dbo.tbl_resmgr_inventory ri
			on ri.Inventory_ID = ril.Inventory_ID
		join seed
			on 1=1
		)
	as  source
	on  source.inventory_id = target.inventory_id
		and source.location_id = target.location_id
	when matched and ( 1=1 )
	then
	update	set target.onhand = source.quantity,
				target.quantity = source.quantity,
				target.loggedby_id = -1,
				target.loggedDate = SYSDATETIME() 
	when not matched by target then
	insert 
	(
				[Inventory_ID]
			   ,[Location_ID]
			   ,[TransactionType_ID]
			   ,[AdjustmentType_ID]
			   ,[Reference_ID]
			   ,[ReferenceType]
			   ,[RecordDate]
			   ,[UnitCost]
			   ,[Quantity]
			   ,[Cost]
			   ,[Rate_ID]
			   ,[ChargeRate]
			   ,[IsEstimated]
			   ,[IsLocked]
			   ,[IsAdHocEntry]
			   ,[IsOverrideRate]
			   ,[AdHocName]
			   ,[Name]
			   ,[Number]
			   ,[ResourceSource]
			   ,[Location]
			   ,[Notes]
			   ,[Field1]
			   ,[Field2]
			   ,[LoggedBy_ID]
			   ,[LoggedDate]
			   ,[IssuedTo_ID]
			   ,[Splr_ID]
			   ,[Lot_ID]
			   ,[OnHand]
			   ,[IsFullfilled]
			   ,[TotalCharge]
			   ,[ExternalID]
			   ,[FromTrans_ID])
		 VALUES
			   (
			   source.inventory_id --<Inventory_ID, int,>
			   , source.location_id --<Location_ID, int,>
			   , 1 --<TransactionType_ID, int,>
			   , -1 --<AdjustmentType_ID, int,>
			   , null --<Reference_ID, int,>
			   , null --<ReferenceType, int,>
			   , SYSDATETIME() -- datetime,>
			   , source.unitCharge --<UnitCost, money,>
			   , source.quantity --<Quantity, decimal(18,4),>
			   , source.unitCharge * source.quantity -- <Cost, money,>
			   , -1 --<Rate_ID, int,>
			   , 0 --<ChargeRate, money,>
			   , 0 --<IsEstimated, bit,>
			   , 0 --<IsLocked, bit,>
			   , 0 --<IsAdHocEntry, bit,>
			   , 0 --<IsOverrideRate, bit,>
			   , '' --<AdHocName, varchar(100),>
			   , source.name --<Name, varchar(100),>
			   , 1 --<Number, varchar(100),>
			   , '' --<ResourceSource, varchar(256),>
			   , '' --<Location, varchar(256),>
			   , '' -- <Notes, varchar(1024),>
			   , '' -- <Field1, varchar(50),>
			   , '' -- <Field2, varchar(50),>
			   , 342 --<LoggedBy_ID, int,>
			   , SYSDATETIME() -- <LoggedDate, datetime,>
			   , null --<IssuedTo_ID, int,>
			   , null -- <Splr_ID, int,>
			   , null -- <Lot_ID, int,>
			   , source.quantity -- <OnHand, decimal(18,4),>
			   , null -- <IsFullfilled, bit,>
			   , null -- <TotalCharge, money,>
			   , null -- <ExternalID, varchar(50),>
			   , null -- <FromTrans_ID, int,>
	);
	------------------------------------------------------------------


COMMIT TRANSACTION [Tran1]

END TRY
BEGIN CATCH
	--TODO: email?
	--get with brian g
  ROLLBACK TRANSACTION [Tran1]
END CATCH  


 
END





GO

sp_updatestat

GO
