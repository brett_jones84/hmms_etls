USE [HMMS]
GO

/****** Object:  View [dbo].[vdot_vw_csc_districts]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('vdot_vw_csc_districts') IS NOT NULL
	PRINT 'dropping view Create_vdot_vw_csc_districts...'
	go

	DROP VIEW  [dbo].[vdot_vw_csc_districts]
GO

PRINT 'Creating VIEW vdot_vw_csc_districts.View...'
go

CREATE VIEW [dbo].[vdot_vw_csc_districts] AS
SELECT distinct
      [districtID]
      ,[districtName]
      ,[districtPhone]
  FROM [HMMS].[dbo].[vdot_vw_csc_residencies]
GO

PRINT '...done'
go
