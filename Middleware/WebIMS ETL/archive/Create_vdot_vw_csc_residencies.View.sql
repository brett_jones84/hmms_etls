USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_residencies] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('vdot_vw_csc_residencies') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_residencies]
GO

--residency/district
CREATE VIEW [dbo].[vdot_vw_csc_residencies] AS
  SELECT r.[vdot_ResidencyCode] residencyCode
	  , r.[vdot_name] residencyName
      , r.[EmailAddress] residencyEmail
      , r.[vdot_ContactName] residencyContactName
      , r.[vdot_ContactPhone] residencyContactPhone
      , r.[vdot_ResidencyAddress] residencyAddress
      , r.[vdot_District] districtID
	  , d.vdot_name districtName
	  , d.vdot_ContactPhone districtPhone
  FROM [CSC_MSCRM].[dbo].[vdot_residency] r
  LEFT JOIN [CSC_MSCRM].[dbo].[vdot_district] d
	on d.vdot_districtId = r.vdot_District;


GO
