USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_stock_locations] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('vdot_vw_stock_locations') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_stock_locations]
GO

CREATE VIEW [dbo].[vdot_vw_stock_locations] as
WITH src as (
SELECT [STOCKLOCATIONCODE]
      ,[STOCKLOCATIONNAME]
      ,[RESIDENCYID]
	  ,SUBSTRING([RESIDENCYID],2,2) rc 
      --,[DISTRICTID]
      ,[ORGCODE]
      ,[LAST_LOADED_DATE]
  FROM [HMMS_Staging].[dbo].[DEB_WEBIMS_TODAY_STOCKLOCATIONS] sl
  )
  Select * from src
  LEFT JOIN [HMMS].[dbo].[vdot_vw_csc_residencies] r 
  ON RTRIM(LTRIM(src.rc)) collate Latin1_General_CI_AI   = 
  RTRIM(LTRIM(r.residencyCode)); 
  

GO
