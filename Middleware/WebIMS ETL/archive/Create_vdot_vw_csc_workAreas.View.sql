USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_workAreas]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('vdot_vw_csc_workAreas') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_workAreas]
GO

CREATE VIEW [dbo].[vdot_vw_csc_workAreas] AS
SELECT [vdot_workareaId] workAreaId
	  ,[vdot_WorkAreaTypeName] workAreaTypeName
      ,[vdot_name] workAreaName
      ,[vdot_AMSWorkAreaID] workAreaAMSId
      ,[vdot_LocationCode] workAreaLocationCode
  FROM [CSC_MSCRM].[dbo].[vdot_workarea];
GO
