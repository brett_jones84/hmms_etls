The following steps will create the necessary objects for the WebIMS import and run the load.  The entire job should run in less than 5 seconds based on WSQ01438 runs.  Note that the job is a full wipe/reload currently and can be run multiple times.
The databases that this script will affect are set in the main.bat file with the following parameters and default values:

MAIN_DB="HMMS" 
STAGING_DB="HMMS_STAGING"
MSCRM="CSC_MSCRM"

If using different databases please change the values accordingly. For the purposes of this document the databases will be referred to by their variable name.

Step 1: Take a backup of the MAIN_DB database.

Step 2: Ensure person executing scripts has the following permissions in the target server databases...

	a) create a table in the STAGING_DB database
	b) edit table data in MAIN_DB and STAGING_DB
	c) create procedure in MAIN_DB
	d) create and assign owner_login_name for job ( https://msdn.microsoft.com/en-us/library/ms187901.aspx ).  The job must also be configured to run as a login that has permissions to execute SQL Agent jobs.
	e) read data from MSCRM database (for views)

Step 3: Ensure that all files are in the same directory:

	Create_and_Populate_StockGroups_Table.sql
	Create_IMSDataLoad_Proc.sql
	Create_vdot_vw_csc_districts.View.sql
	Create_vdot_vw_csc_residencies.View.sql
	Create_vdot_vw_csc_workAreas.View.sql
	Create_vdot_vw_stock_locations.View.sql
	create_vdot_vw_webims_staged_inventory.sql
	create_vdot_vw_webims_staged_inv_at_location.sql
	Create_vdot_vw_xref_nigp_webims.sql
	Create_WebIMSDataLoad_Job.sql	
	main.bat
	main.sql
	

Step 4: Run main.bat (this executes main.sql which calls most of the necessasary steps).  the output from this is appended to results.log

Step 5: In SQL Mgmt Studio, Run Create_WebIMSDataLoad_Job.sql

Step 6: In SQL Mgmt Studio, Execute "WebIMSDataLoad" Job

ROLLBACK:
	Restore the MAIN_DB database back to the backup point taken at the beginning of these instructions.