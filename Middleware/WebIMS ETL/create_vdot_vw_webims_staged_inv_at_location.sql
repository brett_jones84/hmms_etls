USE [$(MAIN_DB)]
GO


IF OBJECT_ID('vdot_vw_webims_staged_inv_at_location') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_webims_staged_inv_at_location]
GO


/****** Object:  View [dbo].[vdot_vw_webims_staged_inv_at_location]    Script Date: 7/5/2017 3:42:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vdot_vw_webims_staged_inv_at_location] AS

WITH sub AS
(
SELECT 
	CASE WHEN SUBSTRING(ti.STOCKNO, 10, 1) = 'N' THEN CAST(ti.STOCKCATALOGID AS VARCHAR(255))+'1'
		WHEN SUBSTRING(ti.STOCKNO, 10, 1) = 'S' THEN CAST(ti.STOCKCATALOGID AS VARCHAR(255))+'3'
		WHEN SUBSTRING(ti.STOCKNO, 10, 1) = 'U' THEN CAST(ti.STOCKCATALOGID AS VARCHAR(255))+'5'
		WHEN SUBSTRING(ti.STOCKNO, 10, 1) = 'X' THEN CAST(ti.STOCKCATALOGID AS VARCHAR(255))+'7'
		WHEN SUBSTRING(ti.STOCKNO, 10, 1) = 'O' THEN CAST(ti.STOCKCATALOGID AS VARCHAR(255))+'9'
		END uncastedId
	, *
FROM HMMS_Staging.dbo.DEB_WEBIMS_TODAY_INVENTORY ti
)
SELECT	CAST(sub.uncastedid AS INT) uniqueInventoryWithConditionId
		,sub.*
FROM sub;


GO