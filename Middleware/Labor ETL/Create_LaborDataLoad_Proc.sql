USE [$(MAIN_DB)]
GO

/****** 
--		Object:  	StoredProcedure [dbo].[LaborLoad]    
--		Author: 	Aric Mueller (WorldView Solutions)
--		History: 	Version 1.0,	8/25/2017, 	Aric Mueller
--					Initial creation of ETL process for pulling employee data from the HMMS_STAGING database.
******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('LaborDataLoad') IS NOT NULL
	BEGIN
		PRINT N'Dropping existing LaborDataLoad Procedure...';  
		DROP PROCEDURE  [dbo].[LaborDataLoad];
	END
GO

--PRINT N'Creating LaborDataLoad Procedure...';  
CREATE procedure [dbo].[LaborDataLoad] 
	@cleanLoad INT = 0 --if 1, wipe any existing data 
as
begin

PRINT N'LaborDataLoad Procedure Beginning...';  
BEGIN TRANSACTION [Tran1]

BEGIN TRY

	IF @cleanLoad = 1
		BEGIN

			PRINT N'cleanLoad param is set.  Purging existing data...';
			
			PRINT N'purging tbl_ResMgr_Personnel_Titles...';  
			DELETE
			FROM tbl_ResMgr_Personnel_Titles;

			PRINT N'purging tbl_ResMgr_Personnel...';  
			DELETE
			FROM tbl_ResMgr_Personnel;

			PRINT N'purging tbl_ResMgr_Personnel_ExData...';  
			DELETE
			FROM [dbo].[tbl_ResMgr_Personnel_ExData]

			PRINT N'purging tbl_ResMgr_Personnel_Rates...';  
			DELETE
			FROM tbl_ResMgr_Personnel_Rates
		END

	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Personnel_Titles  -----------------
	-----------------------------------------------------------------
	--TODO: should we actually be re-loading this?  
	--There is no persistent immutable value(s) to correlate source and target data
	PRINT N'Processing tbl_ResMgr_Personnel_Titles...'; 
	/*
		select distinct dept_id, job_cd, job_title_desc
		from [$(STAGING_DB)].[dbo].[EMPLOYEE_JOB_DATA]
		order by job_title_desc	
	
		SELECT [Dept_ID]
		  ,[Title]
		FROM [HMMS].[dbo].[tbl_ResMgr_Personnel_Titles];
	*/


	insert into tbl_ResMgr_Personnel_Titles (dept_id, title)
	select distinct 18, job_title_desc title
	from [$(STAGING_DB)].[dbo].[EMPLOYEE_JOB_DATA];

	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Personnel  ------------------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Personnel...';  

	set identity_insert dbo.tbl_ResMgr_Personnel ON;

	merge tbl_ResMgr_Personnel as Target
	using (

	SELECT	s.DISTRICTNAME, s.EMP_ID, s.FirstName, s.LastName
					, t.title_id, s.job_title_desc
					, pg.perm_id, pg.name, u.user_ID
			FROM [$(STAGING_DB)].[dbo].[EMPLOYEE_JOB_DATA] s
			LEFT JOIN tbl_ResMgr_Personnel_Titles t
				on t.Dept_ID = 18
				and t.Title = s.JOB_TITLE_DESC
			LEFT JOIN tbl_ResMgr_Permission_Groups pg
				on pg.name = s.DISTRICTNAME
			LEFT JOIN HMMS.dbo.tbl_Users u
				on u.user_name = s.EMAIL_ID
				and u.is_deleted = 0
					) as Source
	on (target.Employee_ID = source.EMP_ID)

	--employee id exists in source and target.  check name and ensure is active
	when matched and (
			target.firstName <> source.firstName
			or target.lastName <> source.lastName
			or target.EmployeeNumber <> source.EMP_ID
			or target.External_Id <> source.EMP_ID
			or target.isActive = 0
			or target.department_id <> 18
			or target.title_id <> source.title_id
			or target.newrecord <> 0
			or target.perm_ID <> source.perm_id
			or target.user_ID <> source.user_ID
		) 
	then
	update	set target.EmployeeNumber = source.EMP_ID
				, target.firstName = source.firstName
				, target.lastName = source.lastName
				, target.fullName = source.firstName + ' ' + source.lastName
				, target.External_Id = source.EMP_ID
				, target.isActive = 1
				, target.department_id = 18
				, target.title_id = source.title_id
				, target.newrecord = 0
				, target.perm_ID = source.perm_ID
				, target.user_ID = source.user_ID
				, target.updatedDate = CURRENT_TIMESTAMP

	--employee id is not yet in target data.  insert.
	when not matched by target then
	insert (Employee_ID, EmployeeNumber, External_ID, FirstName, LastName, fullName, department_id, title_id, isActive, newrecord, perm_ID, user_id, updatedDate)
	values (source.EMP_ID, source.EMP_ID, source.EMP_ID, source.FirstName, source.LastName, source.firstName + ' ' + source.lastName, 18, source.title_id, 1, 0, source.perm_ID, source.user_id, CURRENT_TIMESTAMP)

	--employee id is no longer in source data. set to inactive in target
	when not matched by source then
	update set	target.isActive = 0
				, target.updatedDate = CURRENT_TIMESTAMP
	;

	set identity_insert dbo.tbl_ResMgr_Personnel OFF;

	--select * from tbl_ResMgr_Personnel;
	-----------------------------------------------------------------



	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Personnel_Rates  -----------------
	-----------------------------------------------------------------
	PRINT N'tbl_ResMgr_Personnel_Rates...';  

	merge tbl_ResMgr_Personnel_Rates as Target
	using (
		select employee_id, rate_id, val
		from v_labor_rates 
		) as Source
	on (target.Employee_ID = source.Employee_ID and target.rate_id = source.rate_id)

	--employee id exists in source and target.  check name and ensure is active
	when matched and (
			target.rate != source.val
		) 
	then
	update	set target.rate = source.val

	--employee id is not yet in target data.  insert.
	when not matched by target then
	insert (employee_id, rate_id, rate, available)
	values (source.employee_id, source.rate_id, source.val, CASE WHEN SOURCE.val=0 THEN 0 ELSE 1 END)

	--employee id is no longer in source data. we will not do anything here (TODO: confirm)
	--when not matched by source then
		--delete
	;


	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Personnel_ExData  -----------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Personnel_ExData...';  

	merge tbl_ResMgr_Personnel_ExData as Target
	using (
			SELECT EMP_ID, DEPT_ID, POSITION_ID, JOB_CD, EMP_TYPE_CD, EMAIL_ID, EMAIL_SIZE_NB, DISTRICTNAME
			FROM $(STAGING_DB).DBO.EMPLOYEE_JOB_DATA
		) as Source
	on (target.Employee_ID = source.EMP_ID)

	--employee id exists in source and target.  check name and ensure is active
	when matched and (
			target.Text2 != source.DEPT_ID
			or target.Text3 != source.POSITION_ID
			or target.Text4 != source.JOB_CD
			or target.Text6 != source.EMAIL_ID
			or target.Text7 != source.EMP_TYPE_CD
			or target.Text8 != source.DISTRICTNAME
			or target.Num1 != source.EMAIL_SIZE_NB
		) 
	then
	update	set target.Text2 = source.DEPT_ID
			, target.Text3 = source.POSITION_ID
			, target.Text4 = source.JOB_CD
			, target.Text6 = source.EMAIL_ID
			, target.Text7 = source.EMP_TYPE_CD
			, target.Text8 = source.DISTRICTNAME
			, target.Num1 = source.EMAIL_SIZE_NB

	--employee id is not yet in target data.  insert.
	when not matched by target then
	insert (Employee_ID, Text2, Text3, Text4, Text6, Text7, Text8, Num1)
	values (source.EMP_ID, source.DEPT_ID, source.POSITION_ID, source.JOB_CD, source.EMAIL_ID
			, source.EMP_TYPE_CD, source.DISTRICTNAME, source.EMAIL_SIZE_NB)

	--employee id is no longer in source data. we will not do anything here (TODO: confirm)
	--when not matched by source then
		--delete
	;
 
	PRINT N'Committing transaction...'; 
	COMMIT TRANSACTION [Tran1];

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION [Tran1];

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

	--TODO: email?

	-- return error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
END CATCH  

PRINT N'...LaborDataLoad Complete'; 

END
 
GO


