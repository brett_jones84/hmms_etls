USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_equipment_rates') IS NOT NULL
	DROP VIEW  [dbo].[vdot_equipment_rates]
GO

/****** Object:  View [dbo].[vdot_equipment_rates]    Script Date: 10/18/2017 2:41:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_equipment_rates] AS
	
		SELECT	s.Equipment_ID
				, 1 Rate_ID
				, 'M5 Rate' Rate_name
				, s.[LEASE_TERM]				
				, CASE WHEN s.[LEASE_TERM] = 'HOUR' THEN 0
						ELSE ((s.[LEASE_RATE]/2080)*12)
					END LEASE_RATE
		FROM [$(STAGING_DB)].[dbo].[M5_HMMS_EQUIPMENTS] s
		