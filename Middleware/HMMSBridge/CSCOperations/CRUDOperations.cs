﻿using System;
using System.ServiceModel;
using System.Linq;
using System.Configuration;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using VDOT.CSC.Data.XrmComponents;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Client.Configuration;

using Newtonsoft.Json;




using System.Collections.Generic;
using System.Net;
namespace CSCOperations
{
    /// <summary>
    /// entity operations like add VueWorks ID to Service Request, Update Service Request Status, and Add Comment to Service Request,
    /// </summary>
    /// <remarks>
    /// You can run this from the console for local testing or from the web service project for full testing..</remarks>
    public class CRUDOperations 
    {
        private Guid _vdot_servicerequestid;
        private string connectionName = "MyLocalContext";

        //when set to true, config files must have proper credentials.
        //when set to false, application must be running under an account that has proper credentials.
        //this default value will be overwritten by the isLocal setting in the web.config file.
        private bool isLocal = true;

        //a quick check that will pass an error back up to the api in case the dynamics sdk failed to initialize properly.
        public CRUDOperations_Result initializationResult = new CRUDOperations_Result();

        /// <summary>
        /// Updates the VDOT service request with a new status code. If the status code is the same as the existing status code the change is discarded.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="stringStatusCode"></param>
        /// <returns></returns>
        public CRUDOperations_Result update_VDOT_Status(string vdot_SRID, string stringStatusCode)
        {

            stringStatusCode = stringStatusCode.Replace(" ", "");
            stringStatusCode = stringStatusCode.ToLower();
            int statusCode;
            bool intParseResult = Int32.TryParse(stringStatusCode, out statusCode);


            try
            {
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)

                {
                    int statusCodeFromEnum = -99;
                    //if the statuscode that was passed in was text then use it as the key of the enum.
                    if (!intParseResult)
                    {
                        statusCodeFromEnum = (int)Enum.Parse(typeof(Servicerequest_Statuscode), stringStatusCode);
                    }
                    else
                    {

                        //check to see if the int supplied is actually in the enum
                        if (Enum.IsDefined(typeof(Servicerequest_Statuscode), statusCode))
                        {
                            statusCodeFromEnum = statusCode;
                        }
                        else
                        {
                            throw new System.ArgumentException("Parameter must be a valid value", stringStatusCode);
                        }

                    }

                    ColumnSet cols = new ColumnSet(true);
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    vdot_servicerequest retrievedServicerequest = (vdot_servicerequest)_serviceProxy.Retrieve("vdot_servicerequest", vdot_servicerequstid, cols);

                    if (statusCodeFromEnum != retrievedServicerequest.statuscode.Value)
                    {
                        retrievedServicerequest.statuscode = new OptionSetValue(statusCodeFromEnum);
                        _serviceProxy.Update(retrievedServicerequest);
                        return new CRUDOperations_Result(System.Net.HttpStatusCode.OK, string.Format("Sucess {0} was successfully assigned a new status of {1}({2})", vdot_SRID, stringStatusCode, statusCodeFromEnum));
                    }
                    return new CRUDOperations_Result(System.Net.HttpStatusCode.OK, string.Format("Sucess {0} was NOT assigned a new status of {1}({2}. The service request already has a status of {2})", vdot_SRID, stringStatusCode, statusCodeFromEnum));


                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // You can handle an exception here or pass it back to the calling method.
                return this.HandleExceptions(ex, string.Format("Failure {0} was NOT assigned a new status of {1}", vdot_SRID, stringStatusCode));
                //throw;
            }
        }

        /// <summary>
        /// Updates the VDOT service request with a new status code. If the status code is the same as the existing status code the change is discarded.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="stringStatusCode"></param>
        /// <returns></returns>
        public CRUDOperations_Result update_VDOT_WorkArea(string vdot_SRID, string stringWorkAreaCode)
        {

            try
            {
                using (
                    var _serviceProxy =
                        CrmConfigurationManager.CreateContext(this.connectionName, true) as
                            CrmOrganizationServiceContext)

                {

                    ColumnSet cols = new ColumnSet(true);
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    vdot_servicerequest retrievedServicerequest =
                        (vdot_servicerequest) _serviceProxy.Retrieve("vdot_servicerequest", vdot_servicerequstid, cols);

                    EntityReference workAreaEntityReference = new EntityReference("vdot_workarea", this.getGuidByvdot_WorkAreaID(stringWorkAreaCode));
                    //"404|5"

                    //Guid newGuid = new Guid("74aff68c-3143-e411-80d0-0017a477d446");
                    //EntityReference workAreaEntityReference2 = new EntityReference("vdot_workarea", newGuid);
                    
                    retrievedServicerequest["vdot_amsworkarea"] = workAreaEntityReference;
                   
                    _serviceProxy.Update(retrievedServicerequest);
                    
                   }


                return new CRUDOperations_Result(System.Net.HttpStatusCode.OK,
       string.Format("Sucess {0} was successfully assigned a VueWorks ID of {1}", vdot_SRID, stringWorkAreaCode));

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // You can handle an exception here or pass it back to the calling method.
                return this.HandleExceptions(ex, string.Format("Failure {0} was NOT assigned a new status of {1}", vdot_SRID, "blah"));
                //throw;
            }
        }

        /// <summary>
        /// Updates the servivce request field 'vdot_AMSID' with a new value based on service request id.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="vdot_AMSID"></param>
        /// <returns></returns>
        public CRUDOperations_Result update_VDOT_AMSID(string vdot_SRID, string vdot_AMSID)
        {
            try
            {

                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    ColumnSet cols = new ColumnSet(true);
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    vdot_servicerequest retrievedServicerequest =
                        (vdot_servicerequest)_serviceProxy.Retrieve("vdot_servicerequest", vdot_servicerequstid, cols);
                    retrievedServicerequest.vdot_HMMSINTEGRATIONID = vdot_AMSID;
                    _serviceProxy.Update(retrievedServicerequest);

                    CRUDOperations_Result returnData = new CRUDOperations_Result();
                    returnData.status = System.Net.HttpStatusCode.OK;
                    returnData.text = string.Format("Sucess {0} was successfully assigned a VueWorks ID of {1}",
                        vdot_SRID, vdot_AMSID);
                    //returnData.ServiceRequestData = retrievedServicerequest.ToString();

                    return returnData;


                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // You can handle an exception here or pass it back to the calling method.
                return this.HandleExceptions(ex,
                    string.Format("Failure {0} was NOT assigned a VueWorks ID of {1}", vdot_SRID, vdot_AMSID));
                //throw;
            }
        }


        /// <summary>
        /// Updates the servivce request field 'vdot_HMMSWORKORDERID' with a new value based on service request id.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="HMMS_WO_ID"></param>
        /// <returns></returns>
        public CRUDOperations_Result update_VDOT_WorkOrder_ID(string vdot_SRID, string HMMS_WO_ID)
        {
            try
            {

                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    ColumnSet cols = new ColumnSet(true);
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    vdot_servicerequest retrievedServicerequest =
                        (vdot_servicerequest)_serviceProxy.Retrieve("vdot_servicerequest", vdot_servicerequstid, cols);
                    retrievedServicerequest.vdot_HMMSWORKORDERID = HMMS_WO_ID;
                    _serviceProxy.Update(retrievedServicerequest);

                    CRUDOperations_Result returnData = new CRUDOperations_Result();
                    returnData.status = System.Net.HttpStatusCode.OK;
                    returnData.text = string.Format("Sucess {0} was successfully assigned a VueWorks ID of {1}", vdot_SRID, HMMS_WO_ID);

                    returnData.ServiceRequestData_status = "Assigned";

                    string hmmsSystem;
                    //when 866190000 then 1--'7 - Interstate'
                    //when 866190001 then 2--'5 - Primary'
                    //when 866190002 then 3--'6 - Secondary'
                    switch (retrievedServicerequest.vdot_ServiceAreaType.Value.ToString())
                    {
                        case "866190000":
                            hmmsSystem = "7 - Interstate";
                            break;
                        case "866190001":
                            hmmsSystem = "5 - Primary";
                            break;
                        default:
                            hmmsSystem = "6 - Secondary";
                            break;
                    }
                    

                    returnData.ServiceRequestData_system = hmmsSystem;



                    returnData.ServiceRequestData_routeNumber = retrievedServicerequest.vdot_RouteNumber.ToString();




                        //= string.Format("statuscode : {0},vdot_ServiceAreaType: {1}, vdot_RouteNumber : {2} ", "Assigned","5 - Primary", retrievedServicerequest.vdot_RouteNumber.ToString());


                    //returnData.ServiceRequestData = string.Format("statuscode : {0},vdot_ServiceAreaType: {1}, vdot_RouteNumber : {2} ", retrievedServicerequest.statuscode.Value.ToString(),
                    //    retrievedServicerequest.vdot_ServiceAreaType.Value.ToString(), retrievedServicerequest.vdot_RouteNumber.ToString());



                    //when 866190000 then 1--'Interstate'
                    //when 866190001 then 2--'Primary'
                    //when 866190002 then 3--'Secondary'


                    //returnData.ServiceRequestData = "{" + returnData.ServiceRequestData + "}";

                    return returnData;




                    //return new CRUDOperations_Result(System.Net.HttpStatusCode.OK,
                    //    string.Format("Sucess {0} was successfully assigned a VueWorks ID of {1}", vdot_SRID, HMMS_WO_ID));
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // You can handle an exception here or pass it back to the calling method.
                return this.HandleExceptions(ex,
                    string.Format("Failure {0} was NOT assigned a VueWorks ID of {1}", vdot_SRID, HMMS_WO_ID));
                //throw;
            }
        }

        

        /// <summary>
        /// gets all comments associated with service account id
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="vdot_servicerequstid"></param>
        /// <returns></returns>
        public EntityCollection get_Comments(string vdot_SRID, Guid vdot_servicerequstid = new Guid())
        {
            try
            {
                if (vdot_servicerequstid == Guid.Empty)
                {
                    vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                }
                
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    ColumnSet cols = new ColumnSet(true);
                    QueryByAttribute querybyattribute = new QueryByAttribute("annotation");
                    querybyattribute.ColumnSet = cols;
                    querybyattribute.Attributes.AddRange("objectid");
                    querybyattribute.Values.AddRange(vdot_servicerequstid);
                    EntityCollection retrieved = _serviceProxy.RetrieveMultiple(querybyattribute);
                    return retrieved;
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                //throw the error back up to the calling function
                System.Console.WriteLine(ex);
                throw;
            }

        }

        /// <summary>
        /// Add a comment to a service request. If the comment text is identical to the incoming text the change is discarded.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="commentText"></param>
        /// <param name="timeStamp"></param>
        /// <param name="useDBForCompare"></param>
        /// <returns></returns>
        public CRUDOperations_Result add_Comment(string vdot_SRID, string commentText, Nullable<DateTime> timeStamp, string commentSubject, bool useDBForCompare = true )
        {
            string dupMessage = "";
            try
            {
                Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);

                if (!useDBForCompare)
                {
                    EntityCollection commentsForThisSR = this.get_Comments(vdot_SRID, vdot_servicerequstid);
                    foreach (Annotation entity in commentsForThisSR.Entities)
                    {
                        
                        if (commentText == entity.NoteText)
                        {
                            dupMessage =
                                string.Format(
                                    "A comment was NOT added to SR {0}. A comment with the same text already exists for this service Request.",
                                    vdot_SRID);
                            return new CRUDOperations_Result(System.Net.HttpStatusCode.OK, dupMessage);
                        }
                    }

                }
                
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    ColumnSet cols = new ColumnSet(true);
                    Entity annotation = new Entity("annotation");
                    annotation["objectid"] = new EntityReference("vdot_servicerequest", vdot_servicerequstid);
                    annotation["subject"] = commentSubject;
                    annotation["notetext"] = commentText;
                    Guid annotationId = _serviceProxy.Create(annotation);
                    return new CRUDOperations_Result(System.Net.HttpStatusCode.OK, string.Format("Sucess A comment was added to SR {0}. Comment text :{1}", vdot_SRID, commentText));
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                return this.HandleExceptions(ex, string.Format("Failure a new comment for SR {0} was not created. Comment text : {1}", vdot_SRID, commentText));
            }


        }

        /// <summary>
        /// General utility function that gets a guid based on service record id.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <returns></returns>
        public Guid getGuidByvdot_SRID(string vdot_SRID)
        {

            if (this._vdot_servicerequestid != Guid.Empty)
            {
                return this._vdot_servicerequestid;
            }
            else
            {

                try
                {

                    using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                    {
                        ColumnSet cols = new ColumnSet("vdot_servicerequestid");
                        QueryByAttribute querybyattribute = new QueryByAttribute("vdot_servicerequest");
                        querybyattribute.ColumnSet = cols;
                        querybyattribute.Attributes.AddRange("vdot_srid");
                        querybyattribute.Values.AddRange(vdot_SRID);
                        EntityCollection retrieved = _serviceProxy.RetrieveMultiple(querybyattribute);
                        this._vdot_servicerequestid = retrieved[0].GetAttributeValue<Guid>("vdot_servicerequestid");
                        return this._vdot_servicerequestid;

                    }

                }
                // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
                catch (Exception ex)
                {
                    // pass exception back to the calling method.
                    if (ex is ArgumentException)
                    {
                        throw new ArgumentException(String.Format("{0} is not a valid Service Request ID", vdot_SRID));
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }




        /// <summary>
        /// General utility function that gets a guid based on service record id.
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <returns></returns>
        public Guid getGuidByvdot_WorkAreaID(string workArea_ID)
        {

                try
                {

                    using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                    {
                        ColumnSet cols = new ColumnSet("vdot_workareaid");
                        QueryByAttribute querybyattribute = new QueryByAttribute("vdot_workarea");
                        querybyattribute.ColumnSet = cols;
                        //querybyattribute.Attributes.AddRange("vdot_amsworkareaid");
                        querybyattribute.Attributes.AddRange("vdot_name");
                        querybyattribute.Values.AddRange(workArea_ID);
                        EntityCollection retrieved = _serviceProxy.RetrieveMultiple(querybyattribute);
                        Guid workAreaGuid = retrieved[0].GetAttributeValue<Guid>("vdot_workareaid");
                        return workAreaGuid;

                    }

                }
                // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
                catch (Exception ex)
                {
                    // pass exception back to the calling method.
                    if (ex is ArgumentException)
                    {
                        throw new ArgumentException(String.Format("{0} is not a valid Work Area ID", workArea_ID));
                    }
                    else
                    {
                        throw;
                    }
                }
            
        }




        /// <summary>
        /// Display all the attributes associated with an entity. Useful to get information about entities.
        /// </summary>
        /// <param name="entityName"></param>
        public CRUDOperations_Result displayEntityAttributes(string entityName = "vdot_workarea")
        {
            List<string> entities = new List<string>();
            try
            {
                
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {

                    Microsoft.Xrm.Sdk.Messages.RetrieveEntityRequest req = new Microsoft.Xrm.Sdk.Messages.RetrieveEntityRequest
                    {
                        EntityFilters = Microsoft.Xrm.Sdk.Metadata.EntityFilters.All,
                        LogicalName = entityName // like "account"
                    };
                    Microsoft.Xrm.Sdk.Messages.RetrieveEntityResponse res = (Microsoft.Xrm.Sdk.Messages.RetrieveEntityResponse)_serviceProxy.Execute(req
                    );
                    Microsoft.Xrm.Sdk.Metadata.EntityMetadata currentEntity = res.EntityMetadata;
                    foreach (Microsoft.Xrm.Sdk.Metadata.AttributeMetadata attribute in currentEntity.Attributes)
                    {
                        var attributeName = attribute.LogicalName;
                        entities.Add(attributeName);
                        //System.Console.WriteLine(attributeName);
                        System.Diagnostics.Debug.WriteLine(attributeName);
                    }

                    var entities_json = JsonConvert.SerializeObject(entities);
                    return new CRUDOperations_Result(System.Net.HttpStatusCode.OK, entities_json);
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                //pass the exception back to the calling function
                System.Console.WriteLine(ex);
                throw;
            }

        }

        /// <summary>
        /// getall the options associated with an entity
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="fieldName"></param>
        public void GetOptionSet(string entityName = "vdot_servicerequest", string fieldName = "statuscode")
        {

            try
            {
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {

                    var attReq = new RetrieveAttributeRequest();
                    attReq.EntityLogicalName = entityName;
                    attReq.LogicalName = fieldName;
                    attReq.RetrieveAsIfPublished = true;
                    var attResponse = (RetrieveAttributeResponse)_serviceProxy.Execute(attReq);
                    var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;
                    var optionList = (from o in attMetadata.OptionSet.Options
                                      select new { Value = o.Value, Text = o.Label.UserLocalizedLabel.Label }).ToList();
                    string optionListString = optionList.ToString();

                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // Pass the exception back to the calling method.
                System.Console.WriteLine(ex);
                throw;
            }


        }
        /// <summary>
        /// This gets executed for console testing
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="vdot_AMSID"></param>
        public void Run(string vdot_SRID, string vdot_AMSID)
        {
            try
            {
                this.update_VDOT_AMSID(vdot_SRID, vdot_AMSID);
            }

            catch (Exception ex)
            {
                //pass the excpetion off to the handler.
                this.HandleExceptions(ex, "Error at Run");
            }
            // Additional exceptions to catch: SecurityTokenValidationException, ExpiredSecurityTokenException,
            // SecurityAccessDeniedException, MessageSecurityException, and SecurityNegotiationException.
            finally
            {
                Console.WriteLine("Press <Enter> to exit.");
                Console.ReadLine();
            }


        }
        /// <summary>
        /// initialize the object.
        /// </summary>
        public CRUDOperations()
        {

            try
            {
                try
                {
                    this.isLocal = Convert.ToBoolean(ConfigurationManager.AppSettings["isLocal"].ToString());
                    this.connectionName = this.isLocal
                        ? ((ConnectionNames) 1).ToString()
                        : ((ConnectionNames) 2).ToString();
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine("isLocal setting in the webconfig is not true or false. :  Error:" + ex.ToString());
                }


            }
            catch (Exception ex)
            {
                this.initializationResult = this.HandleExceptions((ex), "Error at object construction.");
            }
        }

        /// <summary>
        /// Normalizes all exceptions and returns a result object.
        /// </summary>
        /// <param name="ex0"></param>
        /// <param name="additionalInfo"></param>
        /// <returns></returns>
        private CRUDOperations_Result HandleExceptions(Exception ex0, string additionalInfo = "")
        {
            CRUDOperations_Result result = new CRUDOperations_Result();
            result.text = additionalInfo + " - " + ex0.Message;
            result.FullMessage = ex0.ToString();

            //catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            if (ex0 is FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>)
            {
                FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex = (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>)ex0;
                result.status = HttpStatusCode.NotFound;
                result.Timestamp = ex.Detail.Timestamp;
                result.ErrorCode = ex.Detail.ErrorCode;
                result.Message = ex.Detail.Message;
                result.InnerFault = null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault";
                return result;
            }

            //catch (System.TimeoutException ex)
            if (ex0 is System.TimeoutException)
            {
                result.status = HttpStatusCode.RequestTimeout;
                System.TimeoutException ex = (System.TimeoutException)ex0;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                result.InnerFault = null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message;
                return result;
            }


            //if the connection string was not valid throw error.
            if (ex0 is ArgumentException)
            {
                ArgumentException ex = (ArgumentException)ex0;
                result.status = HttpStatusCode.NotFound;
                result.Message = ex.Message;
                return result;

            }


            //catch all other errors
            if (ex0 is System.Exception)
            {
                System.Exception ex = ex0;

                result.status = HttpStatusCode.InternalServerError;
                result.Message = ex.Message;
                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    result.InnerFault = ex.InnerException.Message;
                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        result.Timestamp = fe.Detail.Timestamp;
                        result.ErrorCode = fe.Detail.ErrorCode;
                        result.Message = fe.Detail.Message;
                        result.TraceText = fe.Detail.TraceText;
                        result.InnerFault = null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault";
                    }
                }
            }

            // Additional exceptions to catch: SecurityTokenValidationException, ExpiredSecurityTokenException,
            // SecurityAccessDeniedException, MessageSecurityException, and SecurityNegotiationException.

            return result;
        }

        #region Main method
        /// <summary>
        /// Standard Main() method used by most SDK samples.
        /// </summary>
        /// <param name="args"></param>
        static public void Main(string[] args)
        {
            CRUDOperations app = new CRUDOperations();
            app.Run("115710", "19876458");
        }

        #endregion Main method
    }
    /// <summary>
    /// Class for return types. Meant to be consumed by WEB API.
    /// </summary>
    public class CRUDOperations_Result
    {
        public System.Net.HttpStatusCode status { get; set; }
        public string text { get; set; }
        public DateTime Timestamp { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public string TraceText { get; set; }
        public string InnerFault { get; set; }
        public string StackTrace { get; set; }
        public string FullMessage { get; set; }
        public string ServiceRequestData_status { get; set; }
        public string ServiceRequestData_system { get; set; }
        public string ServiceRequestData_routeNumber { get; set; }

        public CRUDOperations_Result(System.Net.HttpStatusCode status = HttpStatusCode.BadRequest, string text = "", DateTime Timestamp = default(DateTime), int ErrorCode = 0, string Message = "", string TraceText = "", string InnerFault = "", string StackTrace = "", string FullMessage = "", string ServiceRequestData_status= "", string ServiceRequestData_system = "",  string ServiceRequestData_routeNumber = "")
        {
            this.status = status;
            this.text = text;
            this.Timestamp = Timestamp;
            this.ErrorCode = ErrorCode;
            this.Message = Message;
            this.TraceText = TraceText;
            this.InnerFault = InnerFault;
            this.StackTrace = StackTrace;
            this.FullMessage = FullMessage;
            this.ServiceRequestData_status = ServiceRequestData_status;
            this.ServiceRequestData_system = ServiceRequestData_system;
            this.ServiceRequestData_routeNumber = ServiceRequestData_routeNumber;
        }
    }

    /// <summary>
    /// Enumeration of values for the Service Request status code
    /// </summary>
    public enum Servicerequest_Statuscode
    {

        [System.Runtime.Serialization.EnumMemberAttribute()]
        notapplicable = 866190010,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        needsreview = 866190006,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        assigned = 1,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        inprocess = 866190009,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        cancelrequested = 866190008,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        cancelled = 866190003,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        pending = 866190004,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        rejected = 866190007,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        closed = 866190005,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        inactive = 2,
    }

    //enumeration of context keys found in the web.config file.
    //you can have different contexts depending on where the application is run.
    public enum ConnectionNames
    {
        [System.Runtime.Serialization.EnumMemberAttribute()]
        MyLocalContext = 1,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MyOnPremiseContext = 2,
    }
}


