﻿using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HMMS_Bridge.Models;
using CSCOperations;
using HMMS_DAL;

namespace HMMS_Bridge.Controllers
{
    /// <summary>
    /// Services for interacting for HMMS 
    /// </summary>
    public class HMMSController : ApiController
    {

        private HMMSEntities db = new HMMSEntities();



        /// <summary>
        /// Gets service requests closures from HMMS.
        /// </summary>
        /// <returns>vdot_vw_hmms_sr_closures</returns>
        // GET: api/HMMS/servicerequests/closures/
        [Route("~/api/HMMS/servicerequests/closures/")]
        public IQueryable<vdot_vw_hmms_sr_closures> Getclosures()
        {
            return db.vdot_vw_hmms_sr_closures;
        }


        /// <summary>
        /// Gets work orders that have a satus different than their csc counterpart.
        /// </summary>
        /// <returns>vdot_vw_hmms_sr_status_changes</returns>
        // GET: api/HMMS/servicerequests/statuschanges/
        [Route("~/api/HMMS/servicerequests/statuschanges/")]
        public IQueryable<vdot_vw_hmms_sr_status_changes> Getstatuschanges()
        {
            return db.vdot_vw_hmms_sr_status_changes;
        }


        /// <summary>
        /// Gets service requests work area updates from HMMS.
        /// </summary>
        /// <returns>vdot_vw_hmms_changedWorkAreas</returns>
        // GET: api/HMMS/servicerequests/workareaupdates/
        [Route("~/api/HMMS/servicerequests/workareaupdates/")]
        public IQueryable<vdot_vw_hmms_changedWorkAreas> GetWorkAreaUpdates()
        {
            return db.vdot_vw_hmms_changedWorkAreas;
        }

        /// <summary>
        /// Returns all HMMS comments from the HMMS database
        /// </summary>
        /// <returns>vdot_vw_sr_comments</returns>
        [Route("~/api/HMMS/servicerequests/Comments")]
        [HttpGet]
        public IQueryable<vdot_vw_sr_comments> HMMSComments()
        {
            return db.vdot_vw_sr_comments;
        }

        /// <summary>
        /// Returns all HMMS 'to customer' work order fields from the HMMS database
        /// </summary>
        /// <returns>vdot_vw_wo_toCustomer</returns>
        [Route("~/api/HMMS/workorders/toCustomer")]
        [HttpGet]
        public IQueryable<vdot_vw_wo_toCustomer> GetToCustomers()
        {
            return db.vdot_vw_wo_toCustomer;
        }


    }
}
