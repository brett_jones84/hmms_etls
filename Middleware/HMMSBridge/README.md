# README #

# VDOT HMMS Bridge

* This solution exposes Microsoft Dynamics SDK functionality through Microsoft API services.
    * Services Exposed:
        * Update a VDOT Service Request with a VueWorks ID
            * POST api/AddVueWorksIDToServiceRequest/{VDOT_SRID}?VW_SRID={VW_SRID}
        * Change the status of a VDOT Service Request
            * POST api/UpdateServiceRequestStatus/{VDOT_SRID}?VDOT_SR_Status={VDOT_SR_Status}
        * Get recent service requests
            * GET api/vdot_servicerequest_v2
        * Add a comment to a Service Request.
            * POST api/Comments
            * Comments body example:
            ```javascript
            {
            "VDOT_SRID": "sample string 1",
            "CommentText": "sample string 2",
            "TimeStamp": "2017-02-06T09:27:11.2705883-05:00"            
            }
            ```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Create a local copy of this solutions and build it with Visual Studio. All neccessary files are managed by the NuGet Package manager.

The microsoft CRM SDK libraries are Managed by NuGet.  If you would like more examples on working with the SDK you may want to download the full [SDK] (https://www.microsoft.com/en-us/download/details.aspx?id=24004)

### Prerequisites

* [Microsoft Dynamics CRM 2011 Sofware Development Kit](https://www.microsoft.com/en-us/download/details.aspx?id=24004)
* Web Api 2
* Entity Framework 6
* Visual Studio 2015 or greater.
* .NET Framework 4.5.2


### Installing

1. Create Application in IIS.

2. Create Application Pool for site.

3. Change Application Pool setting 'Enable 32-Bit Application' to 'true'

4. Change Application Pool Identity to a service account that has the following access:
    * Microsoft Dynamics - The account must also have permissions for read access to the CSC
    * Databases - Read access to the following database tables / views:
        * SCRIBEINTERNAL
            * vdot_servicerequest_v2
            * vdot_servicerequest_temp_all
        *   HMMS
            * vdot_vw_csc_comments
            


5. Change the Web.config file to reflect correct Dynamics site and database servers. 

    * Dynamics default setting is 
    ```
    <add name="MyOnPremiseConnection" connectionString="Url=http://csctst.cov.virginia.gov/CSC/XRMServices/2011/Organization.svc;" />
    ```
    * Database settings
    ```
    <add name="SCRIBEINTERNALEntities" connectionString="metadata=res://*/SCRIBEINTERNALModel.csdl|res://*/SCRIBEINTERNALModel.ssdl|res://*/SCRIBEINTERNALModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=WSQ01438;initial catalog=SCRIBEINTERNAL;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />

    <add name="HMMSEntities" connectionString="metadata=res://*/HMMSModel.csdl|res://*/HMMSModel.ssdl|res://*/HMMSModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=WSQ01438;initial catalog=HMMS;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
    ```

6. Publish HMMS_Bridge project from solution to the file system

7. Copy files from local file system into host system.

8. To validate successful install navigate to the applications 'Examples' page:
    * /Examples
    * execute one of the tests and check the Dynamics page to see if the change propigated. 


## Running the tests

No automated tests as of yet.

### Break down into end to end tests

No end to end tests yet

```
Give an example
```

## Deployment

Additional Deployment notes?

## Built With

* [Microsoft Web API](https://www.asp.net/web-api) 
* [Microsoft Dynamics SDK 2011](https://www.microsoft.com/en-us/download/details.aspx?id=24004)
* [Microsoft Entity Framework](https://msdn.microsoft.com/en-us/library/ee712907(v=vs.113).aspx) 


## Versioning

* Version 1.0 - Update a VDOT Service Request with a VueWorks ID
* Version 2.0 - Replace Dynamics SDK download with NuGet library management.

## Authors

* **Aric Mueller** -  [WorldView Solutions](http://worldviewsolutions.com) aric.mueller@worldviewsolutions.com
* **Michael Kolonay** -  [WorldView Solutions](http://worldviewsolutions.com) michael.kolonay@worldviewsolutions.com

## License



