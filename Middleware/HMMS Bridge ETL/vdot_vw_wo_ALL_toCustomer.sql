USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_wo_ALL_toCustomer') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_wo_ALL_toCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE VIEW [dbo].[vdot_vw_wo_ALL_toCustomer] AS
WITH src as (
SELECT 
WO_M.[ID] HMMS_WO_ID
		,WO.ParentServiceRequestID HMMS_SR_ID
		,SR.RefID CSC_SR_ID
		,'Cancellation Reason' AS SUBJECT
		,WO_M.[Cancellation Reason] AS Text	  
  FROM [$(MAIN_DB)].[reports].[WorkOrderForm_MaintenanceWorkOrderForm] as WO_M
  inner join [$(MAIN_DB)].[reports].[WorkOrders] WO on WO.ID = WO_M.ID
  inner join [$(MAIN_DB)].[reports].ServiceRequests SR on SR.ID = WO.ParentServiceRequestID
  inner join (
		  SELECT DISTINCT [Activity_ID]
  FROM [$(MAIN_DB)].[dbo].[tbl_Maintenance_Activity_Audit]
  WHERE ','+FieldsChanged+',' LIKE '%,Cancellation Reason:,%' and 
  ChangeDate > Dateadd(dd, -5, Getutcdate())
		) sub on sub.Activity_ID = WO_M.ID
  where 
  wo.ParentServiceRequestID != -1 
  AND 
  WO_M.[Cancellation Reason] != '' 

  UNION ALL

  SELECT WO_M.[ID] HMMS_WO_ID
		,WO.ParentServiceRequestID HMMS_SR_ID
		,SR.RefID CSC_SR_ID
		,'Closed Status Reason (To Customer)' AS SUBJECT
		,WO_M.[Closed Status Reason (To Customer)] AS Text	  
  FROM [$(MAIN_DB)].[reports].[WorkOrderForm_MaintenanceWorkOrderForm] as WO_M
  inner join [$(MAIN_DB)].[reports].[WorkOrders] WO on WO.ID = WO_M.ID
  inner join [$(MAIN_DB)].[reports].ServiceRequests SR on SR.ID = WO.ParentServiceRequestID
  inner join (
		  SELECT DISTINCT [Activity_ID]
  FROM [$(MAIN_DB)].[dbo].[tbl_Maintenance_Activity_Audit]
  WHERE ','+FieldsChanged+',' LIKE '%,Closed Status Reason (To Customer):,%' and 
  ChangeDate > Dateadd(dd, -5, Getutcdate())
		) sub on sub.Activity_ID = WO_M.ID
  where 
  wo.ParentServiceRequestID != -1 
  AND 
  WO_M.[Closed Status Reason (To Customer)] != ''

  
    UNION ALL

  SELECT WO_M.[ID] HMMS_WO_ID
		,WO.ParentServiceRequestID HMMS_SR_ID
		,SR.RefID CSC_SR_ID
		,'Open Status Reason (To Customer)' AS SUBJECT
		,WO_M.[Open Status Reason (To Customer)] AS Text	  
  FROM [$(MAIN_DB)].[reports].[WorkOrderForm_MaintenanceWorkOrderForm] as WO_M
  inner join [$(MAIN_DB)].[reports].[WorkOrders] WO on WO.ID = WO_M.ID
  inner join [$(MAIN_DB)].[reports].ServiceRequests SR on SR.ID = WO.ParentServiceRequestID
  inner join (
		  SELECT DISTINCT [Activity_ID]
  FROM [$(MAIN_DB)].[dbo].[tbl_Maintenance_Activity_Audit]
  WHERE ','+FieldsChanged+',' LIKE '%,Open Status Reason (To Customer):,%' and 
  ChangeDate > Dateadd(dd, -5, Getutcdate())
		) sub on sub.Activity_ID = WO_M.ID
  where 
  wo.ParentServiceRequestID != -1 
  AND 
  WO_M.[Open Status Reason (To Customer)] != '' 

 		
    UNION ALL

  SELECT WO_M.[ID] HMMS_WO_ID
		,WO.ParentServiceRequestID HMMS_SR_ID
		,SR.RefID CSC_SR_ID
		,'Response to Customer: (Optional)' AS SUBJECT
		,WO_M.[Response to Customer (Optional)] AS Text	  
  FROM [$(MAIN_DB)].[reports].[WorkOrderForm_MaintenanceWorkOrderForm] as WO_M
  inner join [$(MAIN_DB)].[reports].[WorkOrders] WO on WO.ID = WO_M.ID
  inner join [$(MAIN_DB)].[reports].ServiceRequests SR on SR.ID = WO.ParentServiceRequestID
  inner join (
		  SELECT DISTINCT [Activity_ID]
  FROM [$(MAIN_DB)].[dbo].[tbl_Maintenance_Activity_Audit]
  WHERE ','+FieldsChanged+',' LIKE '%,Response to Customer: (Optional),%' and 
  ChangeDate > Dateadd(dd, -5, Getutcdate())
		) sub on sub.Activity_ID = WO_M.ID
  where 
  wo.ParentServiceRequestID != -1 
  AND 
  WO_M.[Response to Customer (Optional)] != '' 
  )

  select NEWID() ID , * from src

  UNION ALL

  select NEWID() ID , src.HMMS_WO_ID, dups.HMMS_SR_ID, dups.CSC_SR_ID, src.SUBJECT, src.Text from src
  INNER JOIN [dbo].[vdot_vw_hmms_duplicates] as dups on src.HMMS_SR_ID = dups.parent_ID








GO
