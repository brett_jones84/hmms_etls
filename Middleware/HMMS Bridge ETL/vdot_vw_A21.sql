USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21]
GO

/****** Object:  View [dbo].[vdot_vw_A21]    Script Date: 9/18/2017 2:37:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_A21] AS
SELECT   dbo.vdot_vw_A21_Inventory.*
FROM     dbo.vdot_vw_A21_Inventory 

UNION ALL

SELECT   dbo.vdot_vw_A21_Labor.*
FROM     dbo.vdot_vw_A21_Labor 



GO