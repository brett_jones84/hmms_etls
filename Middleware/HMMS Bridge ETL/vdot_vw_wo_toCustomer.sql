USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_wo_toCustomer') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_wo_toCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATe VIEW [dbo].[vdot_vw_wo_toCustomer] AS
SELECT  vw.[ID]
      ,vw.[HMMS_WO_ID]
      ,vw.[HMMS_SR_ID]
      ,vw.[CSC_SR_ID]
      ,vw.[SUBJECT]
      ,vw.[Text]
  FROM [$(MAIN_DB)].[dbo].[vdot_vw_wo_ALL_toCustomer] as vw
  LEFT JOIN [$(MAIN_DB)].dbo.vdot_vw_csc_ALL_comments AS csc 
	ON vw.CSC_SR_ID = csc.CSC_SR_ID COLLATE sql_latin1_general_cp1_ci_as 
	AND
	vw.Text = csc.NoteText COLLATE sql_latin1_general_cp1_ci_as 
WHERE
	vw.Text IS NOT NULL
	AND
	csc.NoteText IS NULL



GO