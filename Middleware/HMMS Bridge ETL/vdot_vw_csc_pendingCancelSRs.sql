USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_pendingCancelSRs') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_pendingCancelSRs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE VIEW [dbo].[vdot_vw_csc_pendingCancelSRs] AS
  SELECT csc_sr.vdot_HMMSIntegrationID AS HMMS_SR_ID,
		 csc_sr.vdot_srid AS CSC_SR_ID,
		replace(replace(replace(replace(replace(isnull(csc_sr.vdot_CancellationReason,'') ,'"','&quot;'),'''','&apos;'),'&','&amp;'),'>','&gt;'),'<','&lt;') AS 
		CancellationReason
  FROM $(MSCRM).dbo.vdot_servicerequest csc_sr  
  LEFT JOIN $(MAIN_DB).reports.ServiceRequests hmms_sr ON csc_sr.vdot_HMMSIntegrationID = hmms_sr.ID
  where csc_sr.ModifiedOn > DATEADD(DD,-5,getutcdate())
	and csc_sr.vdot_srid is not null
    and csc_sr.vdot_SubmittedOn is not null 	
	and isnull( csc_sr.vdot_HMMSIntegrationID, -1 ) <> -1
	and csc_sr.vdot_ServiceRequestType = 866190000 -- this is "MAINTENANCE".
	and (csc_sr.statuscode = 866190008 OR csc_sr.statuscode = 866190003) -- "CancelRequested"
	and hmms_sr.Status != 1
  

  










GO
