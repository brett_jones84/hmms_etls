USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_csc_statusCode_lookup') IS NOT NULL
	DROP TABLE  [dbo].[vdot_csc_statusCode_lookup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[vdot_csc_statusCode_lookup](
	[CSC_Status_Id] [int] IDENTITY (1,1) NOT NULL,
	[StatusName] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[CSC_Status_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET IDENTITY_INSERT [dbo].[vdot_csc_statusCode_lookup] ON 
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190010,	N'Nnotapplicable')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190006,	N'needsreview')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (1,	N'assigned')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190009,	N'inprocess')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190008,	N'cancelrequested')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190003,	N'cancelled')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190004,	N'pending')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190007,	N'rejected')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (866190005,	N'closed')
INSERT [dbo].[vdot_csc_statusCode_lookup] ( [CSC_Status_Id],[StatusName]) VALUES (2,	N'inactive')
SET IDENTITY_INSERT [dbo].[vdot_csc_statusCode_lookup] OFF
